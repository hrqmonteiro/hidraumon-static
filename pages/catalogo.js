import { Col, Badge, Container, Nav, Row, Tab } from "react-bootstrap";
import CatalogCard from "../components/UI/CatalogCard";
import Head from "next/head";

export default function Catalogo() {
  return (
    <>
      <Head>
        <title>Catálogo | Hidraumon Máquinas</title>
      </Head>

      <section id="catalog">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
                Nosso
                <br />
                <strong>Catálogo</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Conheça nossa linha de equipamentos.
                <br />
                Todos os nossos produtos carregam a marca <strong><em>Hidraumon®</em></strong>, que trazem consigo a garantia de: <em><strong>qualidade</strong>, <strong>durabilidade</strong>, <strong>confiança</strong> e <u><strong>originalidade!</strong></u>
                </em>
              </p>
            </Col>
          </Row>
          <Col lg={12}>
            <Row>
              <Col>
                <h6 data-aos="fade-up" data-aos-delay="500"><strong>Categorias</strong></h6>
              </Col>
            </Row>
            <Tab.Container id="left-tabs-example" defaultActiveKey="first">
              <Row>
                <Col sm={3} className="mb-5" data-aos="fade-up" data-aos-delay="700">
                  <Nav variant="pills" className="flex-column">
                    <Nav.Item>
                      <Nav.Link eventKey="first">Arqueadoras de Molas</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="second">
                        Carrinhos de Retirar Câmbio
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="third">
                        Carrinhos de Retirar Rodas Duplas
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="fourth">Cilindros e Bombas</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="fifth">Destalonadores</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="sixth">
                      Desparafusadeiras{" "}
                        <Badge bg="warning" text="dark">
                          <em>Novidade!</em>
                        </Badge>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="seventh">Expansores</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="eighth">Macacos</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="ninth">Prensas</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="tenth">Prensas p/ Cutelaria</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="eleventh">
                        Prensas p/ Ourives{" "}
                        <Badge bg="warning" text="dark">
                          <em>Novidade!</em>
                        </Badge>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="twelfth">Rebitadeiras</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="thirteenth">Saca-Pinos</Nav.Link>
                    </Nav.Item>
                  </Nav>
                </Col>
                <Col sm={9} data-aos="fade-up" data-aos-delay="800">
                  <Tab.Content>
                    <Tab.Pane eventKey="first">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Arqueadoras de Molas</strong></h3>
                          <p>Confira nossa linha de arqueadoras de molas.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-3</em> de <em>3</em> resultados</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Arqueadora de Molas 50 Toneladas"
                            description="Desenvolvida para arquear molas a frio até 31 mm com eficiência e qualidade."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Arqueadora de Molas Universal Dupla-Ação 50 Toneladas"
                            description="Equipamento universal 2 em 1: arqueadora e prensa."
                            description2="Desenvolvida para arquear, ajustar e prensar com eficiência e qualidade."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Arqueadora de Molas 50 Toneladas c/ Auxiliar de 20"
                            description="Desenvolvida para arquear molas a frio até 31 mm com eficiência e qualidade."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="second">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Carrinhos de Retirar Câmbio</strong></h3>
                          <p>Confira nossa linha de carrinhos de retirar câmbio.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-1</em> de <em>1</em> resultado</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Carrinho de Retirar Câmbio c/ Bandeja"
                            description="Desenvolvido para retirar câmbio de caminhões."
                            description2="Possui bandeja articulável, regulagem, inclinação 15 graus e rodas em duas opções: ferro fundido e polipropileno."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="third">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Carrinhos de Retirar Rodas Duplas</strong></h3>
                          <p>Confira nossa linha de carrinhos de retirar rodas duplas.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-1</em> de <em>1</em> resultado</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Carrinho de Retirar Rodas Duplas"
                            description="Desenvolvido para retirar rodas duplas de caminhões e carretas."
                            description2="sistema de roletes para posicionamento dos furos em relação as campanas e regulagem de altura de acordo com o pneu."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="fourth">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Cilindros e Bombas</strong></h3>
                          <p>Confira nossa linha de cilindros e bombas.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-3</em> de <em>3</em> resultados</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Bomba Motorizada 700 Bar"
                            description="Capacidade de 700 BAR, motor de 2 HP, reservatório de óleo de 20 litros/óleo 68w, peso de 55 kg."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Eletro-Hidráulica"
                            description="Consulte mais detalhes com um de nossos vendedores: (17) 98201-0001 ou (17) 3226-9090."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Conjunto de Alinhamento de Eixo"
                            description="Capacidade do cilindro de 100 toneladas, altura de 340mm, pressão da bomba de 700BAR, acionamento por válvula hidropneumática direcional de alavanca..."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="fifth">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Destalonadores</strong></h3>
                          <p>Confira nossa linha de destalonadores.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-1</em> de <em>1</em> resultado</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Destalonador de Pneus"
                            description="Desenvolvido para descolagem de talão em pneus agrícolas, caminhões e similares."
                            description2="Possui conjunto hidráulico com cilindro dupla ação e bomba hidropneumática."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="sixth">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Desparafusadeiras</strong></h3>
                          <p>Confira nossa linha de desparafusadeiras.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-1</em> de <em>1</em> resultado</p>
                          </div>
                        </Col>
                      </Row>

                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Desparafusadeira"
                            description="Controle de aperto realizado através do impulso dado pela chave elétrica, cabeça giratória 360°, tensão trifásica, velocidade de rotação 16 RPM..."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="seventh">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Expansores</strong></h3>
                          <p>Confira nossa linha de expansores.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-4</em> de <em>4</em> resultados</p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Expansor Simples Ação 6 Toneladas"
                            description="Ideal para pequenos reparos em latarias, chassis e monoblocos."
                            description2="Conjunto composto por cilindro hidráulico simples ação com retorno por molas, mangueira de alta resistência e bomba de injeção manual. Acompanha conjunto de canos prolongadores e sapatas."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Expansor Simples Ação 20 Toneladas"
                            description="Ideal para reparos em latarias, chassis e caminhões. Colocação do eixo no lugar, remoção da balança travada e colocação dos pinos de tirante."
                            description2="Conjunto composto por cilindro hidráulico simples ação, retorno por gravidade, mangueira de alta resistência e bo..."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Expansor Dupla Ação 40 Toneladas"
                            description="Ideal para reparos em latarias, chassis e caminhões."
                            description2="Bomba acionamento manual (700 BAR) dupla ação e 2 injetores (40 avanço e 20 retorno). Acessórios: Acessórios: 2 sapatas de avanço, 2 sapatas de retorno, 1 tubo maciço 300mm."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Expansor Dupla-Ação 20 Toneladas"
                            description="Ideal para reparos em latarias, chassis e caminhões."
                            description2="Bomba e acionamento manual (700 BAR) dupla ação e 2 injetores (20 avanço e 10 retorno). Acessórios: 2 sapatas de avanço, 2 sapatas de retorno, 1 tubo de 200mm, 1 tubo de 300mm."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="eighth">
                     <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Macacos</strong></h3>
                          <p>Temos uma linha pesada completa de macacos hidráulicos, macaco garrafa, macaco moleiro, macaco carroceria, macaco contêiner e macaco longa distância.
                            <br />
                            <br />
                          Disponíveis nos modelos manual e hidropneumático, e em várias tonelagens.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-12</em> de <em>12</em> resultados</p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Garrafa Manual 32 Toneladas"
                            description="Desenvolvido para elevação de veículos e cargas, possui injetor com acionamento manual."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Garrafa Hidropneumático 32 Toneladas"
                            description="Desenvolvido para elevação de veículos e cargas, possui injetor com acionamento hidropneumático e manual."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Garrafa Hidropneumático 100 Toneladas"
                            description="Desenvolvido para elevação de veículos e cargas, possui injetor com acionamento hidropneumático e manual."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Moleiro Manual 32 Toneladas - 32400"
                            description="Desenvolvido para utilização em oficinas, postos de molas e similares."
                            description2="Disponível em diversas alturas para atender as variações de chassis e carrocerias."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Moleiro Manual 32 Toneladas - 32600"
                            description="Desenvolvido para utilização em oficinas, postos de molas e similares em geral."
                            description2="Disponível em diversas alturas para atender as variações de chassis e carrocerias. Aci..."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Moleiro Hidropneumático 32 Toneladas"
                            description="Desenvolvido para utilização em oficinas, postos de molas e similares em geral."
                            description2="Disponível em diversas alturas para atender as variações de chassis e carrocerias. Aci.."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Moleiro Hidropneumático 60 Toneladas"
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Longa Distância 32 Toneladas"
                            description="Desenvolvido para utilização em oficinas, postos de molas e similares em geral."
                            description2="Seu grande diferencial é a segurança, por ser um equipamento que possibilita ser usado à distância de 2m, sendo mais seguro que os macacos convencionais."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Carroceria 3 Toneladas"
                            description="Desenvolvido para retirar e sustentar carrocerias e baús de caminhões."
                            description2="Acionamento manual com 1 injetor, 5 pontos de regulagem de altura, pés de apoio em cruz para maior estabilidade e rodas de 9'' de borracha macica."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
<Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Carroceria 6 Toneladas"
                            description="Desenvolvido para retirar e sustentar carrocerias e baús de caminhões."
                            description2="Acionamento manual com 1 injetor, lança com 5 pontos de regulagem de altura e pés de apoio em cruz para maior sustentabilidade e rodas de 12'' de borracha macica."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco Carroceria 12 Toneladas"
                            description="Desenvolvido para retirar e sustentar carrocerias e baús de caminhões."
                            description2="Acionamento manual com 1 injetor, 5 pontos de regulagem de altura, pés de apoio em cruz para maior estabilidade e rodas de 12'' de borracha macica."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                            />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Macaco de Sustentação 20 Toneladas"
                            description="Desenvolvido para sustentar carretas e contêineres."
                            description2="Acionamento manual com 1 injetor, com 3 pontos de regulagem de altura e pés de apoio com rodas de 9'' de borracha macica."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                            />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="ninth">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Prensas</strong></h3>
                          <p>Temos uma linha completa de prensas para diversas aplicações de prensagem, modelagem ou corte.
                            <br />
                            <br />
                          Para mais informações, orçamentos, adaptações e projetos especiais entre em contato</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-11</em> de <em>11</em> resultados</p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Hidropneumática 30 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                            />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Manual 15 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Manual 30 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Manual NR-12 200 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Manual 60 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Manual 100 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Manual 200 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Motorizada 30 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Motorizada 45 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Motorizada 100 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa Motorizada NR-12 100 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="tenth">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Prensas p/ Cutelaria</strong></h3>
                          <p>Confira nossa linha de prensas p/ cutelaria.</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-2</em> de <em>2</em> resultados</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa p/ Cutelaria 35 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                            />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa p/ Cutelaria 45 Toneladas"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                            />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="eleventh">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Prensas p/ Ourives</strong></h3>
                          <p>Confira nossa linha de prensas p/ ourives!</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-1</em> de <em>1</em> resultado</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Prensa p/ Ourives"
                            description="As informações técnicas variam de acordo com a tonelagem e acionamento do produto."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="twelfth">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Rebitadeiras</strong></h3>
                          <p>Confira nossa linha de rebitadeiras!</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-1</em> de <em>1</em> resultado</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Rebitadeira Hidropneumática"
                            description="Desenvolvida para rebitar e sacar todos tipos de lonas de freio."
                            description2="Acionamento pneumático, acompanha 5 tipos de punção, sendo 3 para rebitar e 2 para sacar, com filtro regulador de pressão e manômetro."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="thirteenth">
                      <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className="mb-3">
                          <h3><strong>Saca-Pinos</strong></h3>
                          <p>Confira nossa linha de saca-pinos!</p>
                          <div class="exhibition">
                            <p>Exibindo <em>1-1</em> de <em>1</em> resultado</p>
                          </div>
                        </Col>
                      </Row>
                      <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Saca-Pino da Manga de Eixo Manual 45 Toneladas"
                            description="Desenvolvido para sacar pino da manga de eixo de caminhões diretamente no veículo."
                            description2="Com retorno por gravidade, bomba hidráulica manual e 2 injetores (rápido e lento). Disponível nos modelos manual e hidropneumático."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
                          <CatalogCard
                            title="Saca-Pino da Manga de Eixo HidroPneumático 45 Toneladas"
                            description="Desenvolvido para sacar pino da manga de eixo de caminhões diretamente no veículo, acionamento manual e hidropneumático."
                            image="/images/products/arqueadoras/50-toneladas.webp"
                          />
                        </Col>
                      </Row>
                    </Tab.Pane>
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
          </Col>
        </Container>
      </section>
    </>
  );
}
