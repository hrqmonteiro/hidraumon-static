import Head from "next/head";
import Slider from "../components/sections/Slider";
import Solutions from "../components/sections/Solutions";
import Location from "../components/sections/Location";

export default function Home() {
  return (
    <>
      <Head>
        <title>Hidraumon Máquinas</title>
        <meta name="description" content="Hidraumon Máquinas" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Slider />
        <div id="bg-3">
          <img src="/images/bg3.svg" width="1920" height="134" className="img-fluid" alt="" />
        </div>
        <Solutions />
        <div id="bg-4">
          <img src="/images/bg4.svg" width="1920" height="288" className="img-fluid" alt="" />
        </div>
        <Location />
      </main>
    </>
  );
}
