import React, { Component } from "react";
import { Container, Col, Row } from "react-bootstrap";
import Slider from "react-slick";
import Card1 from "../Card1";

export default class EmblaCarousel extends Component {
  render() {
    const settings = {
      className: "center",
      centerMode: true,
      infinite: true,
      centerPadding: "0px",
      slidesToShow: 3,
      speed: 500,
    };

    return (
      <>
        <Row className="m-0 p-0" id="embla-carousel">
          <Col lg={12} md={12} sm={12} xs={12} data-aos="fade-up" data-aos-delay="1500">
            <Slider {...settings}>
              <div className="card-1" id="leve">
                <Card1 title="Automotiva Leve" description="Soluções completas para aplicações desde oficinas pequenas até fábricas multinacionais!" />
              </div>
              <div className="card-1" id="pesada">
                <Card1 title="Automotiva Pesada" description="Temos uma linha completa de soluções para elevações de automóveis pesados" />
              </div>
              <div className="card-1" id="aviacao">
                <Card1 title="Aviação" description="Também possuímos uma gama de equipamentos para aplicações aéreas" />
              </div>
              <div className="card-1" id="industrial">
                <Card1 title="Industrial" description="As melhores e mais completas soluções hidráulicas para aplicações industriais!" />
              </div>
              <div className="card-1" id="naval">
                <Card1 title="Naval" description="Equipamentos de alta resistência para diversas aplicações no setor!" />
              </div>
              <div className="card-1" id="usina">
                <Card1 title="Usinas" description="Possuímos soluções desde cilindros e bombas até maquinário pesado, todos pensados para trazer as melhores soluções!" />
              </div>
            </Slider>
          </Col>
        </Row>
      </>
    );
  }
}
