import { ImWhatsapp, ImPhone } from "react-icons/im";
import { RiQuestionAnswerLine, RiHeadphoneFill } from "react-icons/ri";
import { CgClose } from "react-icons/cg";
import { useState } from "react";
import { Button, Modal } from "react-bootstrap";

export default function WhatsApp() {
  const current = new Date();
  const hours = `${current.getHours()}`
  const date = `${current.getDay()}`

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);

  const [duvidas, setDuvidas] = useState(false);

  const handleDuvidas = () => setDuvidas(false);

  return (
    <>
      {!duvidas && (
        <div id="whatsapp-duvidas">
          <div>Precisando de ajuda?</div>
          <br />
          <div>
            <span id="talk">Fale com nossa equipe!</span>
          </div>
        </div>
      )}

      <div id="whatsapp">
        {show ? (
          <button
            onClick={() => {
              setShow(!show);
              setDuvidas(!duvidas);
            }}
          >
            <CgClose />
          </button>
        ) : (
          <button
            onClick={() => {
              setShow(!show);
              setDuvidas(!duvidas);
            }}
          >
            <ImWhatsapp />
          </button>
        )}
      </div>
      {show && (
        <>
          <div id="modal-1" show={show} onHide={handleClose} data-aos="fade-up">
            <div>
              <RiQuestionAnswerLine />
            </div>
            <div>
              <h5>
                Olá, seja <strong>bem-vindo!</strong>
              </h5>
              <p>
                Para <em>dúvidas</em>, <em>informações</em>, <em>orçamentos</em>{" "}
                etc, selecione uma das opções abaixo para iniciar um atendimento
                com nossa equipe!
              </p>
            </div>
          </div>
          <div id="modal-2" show={show} onHide={handleClose} data-aos="fade-up">
            <a
              href="https://wa.me/5517982010001"
              target="_blank"
              rel="noreferrer"
              className="card-pic"
              data-aos="fade-up"
            >
              <div class="wtpp-info">
                <div>
                  <img src="/images/faby.png" width="45" height="45" alt="Fabiana - Vendas" />
                </div>
                <div>
                  <strong>Fabiana </strong>
                  {hours > 7.30 && hours < 17.30 && date != 0 && date != 6
                  ? <span id="online">Online <RiHeadphoneFill /></span>
                  : <span id="offline">Offline <RiHeadphoneFill /></span>
                  }
                  <br />
                  <p>Setor de Vendas</p>
                </div>
              </div>
              <div id="wtpp-logo">
                <ImWhatsapp />
              </div>
            </a>
            <a
              href="tel:(17)3226-9090"
              target="_blank"
              rel="noreferrer"
              className="card-pic"
              data-aos="fade-up"
              data-aos-delay="200"
            >
              <div class="wtpp-info">
                <div>
                  <img src="/images/hidr.jpg" width="45" height="45" alt="Hidraumon - Direção" />
                </div>
                <div>
                  <strong>Hidraumon </strong>
                  {hours > 7.30 && hours < 17.30 && date != 0 && date != 6
                  ? <span id="online">Online <RiHeadphoneFill /></span>
                  : <span id="offline">Offline <RiHeadphoneFill /></span>
                  }
                  <br />
                  <p>Direção </p>
                </div>
              </div>
              <div id="phone-icon">
                <ImPhone />
              </div>
            </a>

          </div>
        </>
      )}
    </>
  );
}
