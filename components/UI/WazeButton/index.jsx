import {FaWaze} from "react-icons/fa"

export default function WazeButton(props) {
  return (
    <>
      <a href={props.url} target="_blank" rel="noreferrer">
        <button className="waze-button">
          <div id="icon">
            <FaWaze />
          </div>
          <span>
            Abrir no <strong>Waze</strong>
          </span>
        </button>
      </a>
    </>
  );
}
